import Vue from 'vue';
import VueRouter from 'vue-router'
import App from '../components/App';
import Dashboard from '../components/Dashboard';
import NewEmployee from '../components/NewEmployee';
import ViewEmployee from '../components/ViewEmployee';
import EditEmployee from '../components/EditEmployee';
import Login from '../components/Login';
import Register from '../components/Register';
import PrivateChat from '../components/PrivateChat';
import ChatApp from '../components/ChatApp';
import firebase from 'firebase';

Vue.use(VueRouter)

const routes = [

    {
        path: "/", name: "", component: App, children: [

        {
            path:'/twilio',
            name:'ChatApp',
            component:ChatApp,

        },

        {
            path: '/login',
            name: 'login',
            component: Login,
            meta:{
                requiresGuest:true
            }
        },
        {
            path:'/register',
            name:'register',
            component:Register,
            meta:{
                requiresGuest:true
            }
        },
        {
            path: '/',
            name: 'dashboard',
            component: Dashboard,
            meta:{
                requiresAuth:true
            }
        },
        {
            path: '/new',
            name: 'new-employee',
            component: NewEmployee,
            meta:{
                requiresAuth:true
            }
        },
        {
            path: '/auth/:employee_id',
            name: 'view-employee',
            component: ViewEmployee,
            props:true,
            meta:{
                requiresAuth:true
            }
        },
        {
            path: '/edit/:empliyee_id',
            name: 'edit-employee',
            component: EditEmployee,
            props:true,
            meta:{
                requiresAuth:true
            }

        },
        {
            path:'/chat',
            name:'PrivateChat',
            component: PrivateChat,
            meta:{
                requiresAuth:true
            }

        },
      ]
    },
];


export const router = new VueRouter({
    mode: "history",
    routes
});
// //Navbar Gaurd
router.beforeEach((to, from, next) => {
    // Check for requiresAuth guard
    if (to.matched.some(record => record.meta.requiresAuth)) {
      // Check if NO logged user
      if (!firebase.auth().currentUser) {
        // Go to login
        next({
          path: '/login',
          query: {
            redirect: to.fullPath
          }
        });
      } else {
        // Proceed to route
        next();
      }
    } else if (to.matched.some(record => record.meta.requiresGuest)) {
      // Check if NO logged user
      if (firebase.auth().currentUser) {
        // Go to login
        next({
          path: '/',
          query: {
            redirect: to.fullPath
          }
        });
      } else {
        // Proceed to route
        next();
      }
    } else {
      // Proceed to route
      next();
    }
  });

export default router;
